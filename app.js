var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var admicon  = require('./routes/administrador/consultar');
var admicrea = require('./routes/administrador/create');
var admidel = require('./routes/administrador/delete');
var admiup = require('./routes/administrador/update');

var consultar  = require('./routes/cliente/consultar');
var create = require('./routes/cliente/create');
var borrar = require('./routes/cliente/delete');
var actualizar = require('./routes/cliente/update');

var modcon = require('./routes/modelador/consultar');
var modcrea = require('./routes/modelador/create');
var moddel = require('./routes/modelador/delete');
var modup = require('./routes/modelador/update');


var dia = require('./routes/dia/consultar');
var dia1 = require('./routes/dia/create');
var dia2 = require('./routes/dia/delete');
var dia3 = require('./routes/dia/update');

var preciocon = require('./routes/precio/consultar');
var preciocre = require('./routes/precio/create');
var preciodel = require('./routes/precio/delete');
var precioup = require('./routes/precio/update');


var tipocon = require('./routes/tipoTrabajo/consultar');
var tipocre = require('./routes/tipoTrabajo/create');
var tipodel = require('./routes/tipoTrabajo/delete');
var tipoup = require('./routes/tipoTrabajo/update');




var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/administrador/consultar', admicon);
app.use('/administrador/create', admicrea);
app.use('/administrador/delete', admidel);
app.use('/administrador/update', admiup);

app.use('/cliente/consultar', consultar);
app.use('/cliente/create', create);
app.use('/cliente/delete', borrar);
app.use('/cliente/update', actualizar);

app.use('/modelador/consultar', modcon);
app.use('/modelador/create', modcrea);
app.use('/modelador/delete', moddel);
app.use('/modelador/update', modup);

app.use('/dia/consultar', dia);
app.use('/dia/create', dia1);
app.use('/dia/delete', dia2);
app.use('/dia/update', dia3);

app.use('/precio/consultar', preciocon);
app.use('/precio/create', preciocre);
app.use('/precio/delete', preciodel);
app.use('/precio/update', precioup);

app.use('/tipoTrabajo/consultar', tipocon);
app.use('/tipoTrabajo/create', tipocre);
app.use('/tipoTrabajo/delete', tipodel);
app.use('/tipoTrabajo/update', tipoup);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
